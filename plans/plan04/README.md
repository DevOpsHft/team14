# Pushing Image to Registry

Now the goal is to push the created image to a registry, to be available publicly.

## Steps

Do the following:

* Copy the modified `.gitlab-ci.yml` file as well.

## Result

Your created image should be available in the central repository. Find it there! (Hint: the URL should look similar to `https://gitlab.com/DevOpsHft/devopshft/team14/latest`)

### Optional (play kids with enough time)

So try it out (in a docker environment):

`docker container run -p 8080:80 -d registry.gitlab.com/devopshft/<yourrepo>:<yourtag>`

To get access to that environment, ask the workshop leader how.

That means:

* Start a new docker container: `docker container run`
* on port 8080: `-p 8080`
* (by mapping port 80): `:80`
* in the background: `-d `
* with image: `registry.gitlab.com/devopshft/<yourrepo>:<yourtag>`

Try to connect to that running container with:

`docker exec -it <name|ID> bash`

See if you can find the deployed artifacts in the running container.

## Remarks

* Have a look at the [documentation about container registries](https://docs.gitlab.com/ee/user/packages/container_registry/).
* I have enabled some "tricks" that should speed-up the build (if the [documentation](https://gitlab.com/help/ci/docker/using_docker_build.md) is correct)